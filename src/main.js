import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 配置全局文件&样式
import '@/utils/rem.js'
import '@/utils/filters.js'
import '@/assets/css/reset.scss'
// 全局配置swiper
import VueAwesomeSwiper from 'vue-awesome-swiper'
// import style
import 'swiper/swiper.scss'
// 导入vant组件库
// import Vue from 'vue';
import Vant from 'vant';
import 'vant/lib/index.css';

// 导入搜索
import { Search } from 'vant';
Vue.use(Search);

import { Sidebar, SidebarItem } from 'vant';
Vue.use(Sidebar);
Vue.use(SidebarItem);


import { Tabbar, TabbarItem } from 'vant';
Vue.use(Tabbar);
Vue.use(TabbarItem);
// 商品卡片
import { Card } from 'vant';
Vue.use(Card);
//宫格
import { Grid, GridItem } from 'vant';
Vue.use(Grid);
Vue.use(GridItem);
//导航栏
import { NavBar } from 'vant';
Vue.use(NavBar);

import { Lazyload } from 'vant';
Vue.use(Lazyload);
// 注册时可以配置额外的选项
Vue.use(Lazyload, {
    lazyComponent: true,
});



// 引入省市区选择
import { Area } from 'vant';
// 引入省市区选择结束
import { Switch } from 'vant';



import { AddressEdit } from 'vant';

Vue.use(AddressEdit);

Vue.use(Switch);
Vue.use(Vant);
Vue.use(Area);
// 导入vant结束
Vue.use(VueAwesomeSwiper, /* { default options with global component } */)
Vue.config.productionTip = false

// 导航守卫(必须登录成功后才可进入后台系统)
router.beforeEach((to, from, next) => {
    // to是新数据
    // from是旧数据
    // next是跳转的新地址（重定向）
    // console.log("路由变化了");
    // console.log(to.path);
    // console.log(from.path);
    // next()
    if (to.path == "/login") {
        next()
    } else {
        let token = localStorage.getItem('token')
        if (token) {
            next()
        } else {
            next("/login")
        }
    }
})

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
