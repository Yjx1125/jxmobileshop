import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('./views/Home.vue')
        },
        {
            path: '/home1',
            component: () => import('./views/Home1.vue')
        },
        {
            path: '/sort',
            component: () => import('./views/sort.vue')
        },
        {
            path: '/article',
            component: () => import('./views/article.vue')
        },
        {
            path: '/details',
            component: () => import('./views/details.vue')
        },
        // {
        //   path: '/register',
        //   component: () => import('./views/Register.vue')
        // },
        {
            path: '/collect',
            component: () => import('./views/memeber/Collect.vue')
        },
        {
            path: '/address',
            component: () => import('./views/memeber/Address.vue')
        },
        {
            path: '/addressMan',
            component: () => import('./views/memeber/AddressMan.vue')
        }, {
            path: '/my/main',
            component: () => import('./views/vip/Vip.vue')
        }, {
            path: '/orders',
            component: () => import('./views/vip/Orders.vue')
        }, {
            path: '/login',
            component: () => import('./views/vip/Login.vue')
        },

        {
            path: '/shoplist',
            component: () => import('./views/shoplist.vue')
        },
        {
            path: '/shopxq',
            component: () => import('./views/shopxq.vue')
        },

        {
            path: '/pay',
            component: () => import('./views/pay/Index.vue')
        },
        {
            path: '/order/main',
            component: () => import('./views/order/Main.vue')
        }

        // {
        //   path: '/about',
        //   name: 'about',
        //   // route level code-splitting
        //   // this generates a separate chunk (about.[hash].js) for this route
        //   // which is lazy-loaded when the route is visited.
        //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        // }
    ]
})
