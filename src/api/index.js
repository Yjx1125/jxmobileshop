import axios from 'axios'; //导入axios 模块
// import request from './request'
import qs from "qs"

//后期所有请求都会加这个前缀
axios.defaults.baseURL = "http://kg.zhaodashen.cn/v1/"

//通过请求拦截器统一设置token
axios.interceptors.request.use(function (config) {
    config.headers['token'] = localStorage.getItem('token') || 'adf7cbdcdc62b07d94f86339e5687ca51'
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

//收获地址列表
export let getOrderApi = () => {
    return axios.get('address/index.jsp', { params: { token: 'adf7cbdcdc62b07d94f86339e5687ca51' } })
        .then(res => res.data)
}

//商品数据
export let getGoodsAPi = () => {
    return axios.get('cart/index.jsp', { params: { token: 'adf7cbdcdc62b07d94f86339e5687ca51' } })
        .then(res => res.data)
}




// 后期所有请求地址都会加这个前缀,固定，因为地址太长，分开
axios.defaults.baseURL = 'http://kg.zhaodashen.cn/v1/'

//通过请求拦截器统一设置toKen
axios.interceptors.request.use(function (config) {
    config.headers['token'] = localStorage.getItem('token') || 'adf7cbdcdc62b07d94f86339e5687ca51'
    return config
}, function (error) {
    //请求错误后
    return Promise.reject(error);
})
//用户登录
export const postLoginApi = postData => {
    return axios.post('public/login.jsp', qs.stringify(postData)).then(res => res.data)
}

export const getspqqApi = params => {
    return axios.get("goods/index.jsp", { params }).then(res => res.data)
}

export const getListApi = params => {
    return axios.get("goods/index.jsp", { params }).then(res => res.data)
}

export const getDetailsApi = params => {
    return axios.get("goods/detail.jsp", { params }).then(res => res.data)
}
//订单列表接口
// export const getOrdersApi = params => {
//     // let token = localStorage.getItem("token") || 'adf7cbdcdc62b07d94f86339e5687ca51'
//     return axios.get('goods/index.jsp').then(res => res.data)
// }