// 1.导入axios模块
import axios from "axios"
import qs from "qs"
// 添加请求拦截器
axios.interceptors.request.use(function (config) {

    config.headers['token'] = localStorage.getItem("token") || 'adf7cbdcdc62b07d94f86339e5687ca51'
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});
// 3.配置
axios.defaults.baseURL = 'http://kg.zhaodashen.cn/v1/';
// 2.导出封装的异步请求代码
// 收货地址列表
export const getAddressApi = () => {
    let token = localStorage.getItem("token") || 'adf7cbdcdc62b07d94f86339e5687ca51'
    // 将token转为对象
    return axios.get("address/index.jsp", { params: { token } }).then(res => res.data)
}
// 创建收货地址
export const postAddressApi = postData => {
    let token = localStorage.getItem("token") || 'adf7cbdcdc62b07d94f86339e5687ca51'
    return axios.post("address/create.jsp", qs.stringify({ token }, postData)).then(res => res.data)
}
// // 用户删除
// export const deleteUserApi = params => {
//     return axios.delete("users/delete.php", { params }).then(res => res.data)
// }
// // 用户列表冻结(状态切换)
// export const putStateApi = postData => {
//     return axios.put("users/state.php", qs.stringify(postData)).then(res => res.data)
// }
// // 角色列表
// export const getRolesApi = params => {
//     return axios.get("roles/index.php", { params }).then(res => res.data)
// }
// // 角色删除
// export const deleteRolesApi = params => {
//     return axios.delete("roles/delete.php", { params }).then(res => res.data)
// }
// // 权限列表
// export const getAuthApi = params => {
//     return axios.get("auth/index.php", { params }).then(res => res.data)
// }
// // 商品分类列表
// export const getCateApi = params => {
//     return axios.get("cate/index.php", { params }).then(res => res.data)
// }
// // 商品属性列表
// export const getGoodsAttrApi = params => {
//     return axios.get("goods/type/index.php", { params }).then(res => res.data)
// }
// // 商品属性删除
// export const deleteAttrApi = params => {
//     return axios.delete("goods/attr/delete.php", { params }).then(res => res.data)
// }
// // 商品类型状态切换
// export const putTypeApi = postData => {
//     console.log(postData)
//     return axios.put("goods/type/state.php", qs.stringify(postData)).then(res => res.data)
// }
// // 商品管理列表
// export const getGoodsIndexApi = params => {
//     return axios.get("goods/index.php", { params }).then(res => res.data)
// }


// // 添加
// // 商品类型创建
// export const postGoodsTypeApi = params => {
//     return axios.post("goods/type/create.php", { params }).then(res => res.data)
// }
// // 角色创建
// export const postRolesApi = postData => {
//     return axios.post("roles/create.php", qs.stringify(postData)).then(res => res.data)
// }
// // 密保登录
// export const postLoginApi = postData => {
//     return axios.post("users/login.php", qs.stringify(postData)).then(res => res.data)
// }
// // 导航自动
// export const getNavApi = () => {
//     return axios.get("auth/menu.php").then(res => res.data)
// }